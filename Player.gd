extends CharacterBody2D


const SPEED = 300.0
const JUMP_VELOCITY = -400.0
const MAXIMUM_JUMP = 2
const DOUBLE_JUMP_VELOCITY = -200

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
@onready var _animation_sprite = $AnimatedSprite2D
var jump_made = 0

func _process(delta):
	if Input.is_action_pressed("ui_accept"):
		_animation_sprite.play("jump")
	elif Input.is_action_pressed("ui_left"):
		_animation_sprite.scale = Vector2(-1, 1)
		_animation_sprite.play("walk")
	elif Input.is_action_pressed("ui_right"):
		_animation_sprite.scale = Vector2(1, 1)
		_animation_sprite.play("walk")
	else:
		_animation_sprite.play("idle")

func _physics_process(delta):
	# Add the gravity.
	var is_falling := not is_on_floor() and velocity.y > 0.0
	var is_jumping := Input.is_action_just_pressed("ui_accept") and is_on_floor()
	var is_double_jump := Input.is_action_just_pressed("ui_accept") and is_falling
	var is_jump_canceled := Input.is_action_just_released("ui_accept") and velocity.y < 0.0
	if not is_on_floor():
		velocity.y += gravity * delta
	if is_on_floor():
		jump_made = 0

	# Handle Jump.
	if is_jumping:
		jump_made += 1
		velocity.y = JUMP_VELOCITY
	# Handle Double Jump
	elif is_double_jump:
		jump_made += 1
		if jump_made <= MAXIMUM_JUMP:
			velocity.y = DOUBLE_JUMP_VELOCITY
	elif is_jump_canceled:
		velocity.y = 0.0

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

	move_and_slide()
